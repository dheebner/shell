#!/bin/bash

#check for arguments
if [ $# -eq 0 ]; then
  echo "Missing paramter!"
  echo "Usage: $0 /path/to/domainlist/file"
  exit
fi
DIGBIN=$(which dig)
DOMAINLIST=$1

while read -r DOMAIN; do
  #echo $DOMAIN
  DIGOUT=$($DIGBIN @8.8.8.8 -t NS $DOMAIN +short)
  echo $DOMAIN,$(echo $DIGOUT | cut -d' ' -f1)
done < $DOMAINLIST
