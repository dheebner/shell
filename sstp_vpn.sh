#!/bin/bash

# sstp_vpn.sh
# This script will start/stop the sstpc program to initiate a SSTP VPN tunnel

start() {
  #get username (display input)
  read -er -p "Enter VPN Username: " USERNAME

  # get password (hide input with -s option)
  read -ers -p "Enter VPN Password: " PASSWORD

  printf "\nBringing up tunnel to $VPNHOST "

  sstpc --user $USERNAME --password $PASSWORD --log-stdout --cert-warn $VPNHOST > /var/log/sstp.log &

  unset IPADDRESS

  #wait for tunnel to come up before continuing
  for i in $(seq 1 60)
  do
    printf "."
    sleep 1
    # get the IP address from the ppp0 interface; then once we are able to get the IP,
    # then we can get the remote gateway
    IPADDRESS=$(ifconfig | grep -A1 ppp0 | grep inet | tr -s [:blank:] | cut -d" " -f 3)
    # use the -n test to see if the IPADDRESS var is not empty
    if [ -n "$IPADDRESS" ]; then
      VPNGW=$(ifconfig | grep -A1 ppp0 | grep inet | tr -s [:blank:] | cut -d" " -f 7)
      break
    fi
  done

  printf "\nGot IP: %s \n" $IPADDRESS

  read -er -p "If you want to set a route, type the CIDR, otherwise just hit ENTER to continue: " VPNROUTE
  if [ -n "$VPNROUTE" ]; then
    printf "Adding route %s to gateway %s \n" "$VPNROUTE" "$VPNGW"
    ip route add "$VPNROUTE" via "$VPNGW"
  fi
}

stop() {
  killall sstpc
}

VPNHOST="$1"

case "$2" in
start)
    start
    ;;
stop)
    stop
    ;;
*)
    echo "Usage: "$0" {start hostname | stop}"
    exit 1
esac
